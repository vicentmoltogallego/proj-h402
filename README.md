# PROJ-H402



## Stereo capturing by plenoptic and 2D cameras

This repository contains the script developed for the course PROJ-H402 Computing Project, in the ULB.

It only contains one file: multiview2lenslet1.py.

It is a Python script which takes as input several multiview images and puts them together, creating a lenslet 1.0 image.

As it is, it is ready to be downloaded and executed with the following command:

```
python3 multiview2lenslet1.py
```

Note that for it to finish its execution succesfully, one needs to place the images with the right names (i.e. the images in the Input files folder) in the same directory as the script, or modify the script accordingly to read the images one wishes and transform into lenslet 1.0.

It is prepared to read a total of 75 multiview images, create a total of 3 lenslet images formed by 25 multiview images each, and save them into their corresponding files. It will also output the average PSNR of the RVS-synthesized multiview images with respect to the original RLC-synthesized multiview images, as well the PSNR of the lenslet image create from the RVS-synthesized multiview images with respect to the lenslet image created from the original RLC-synthesized multiview images. 

The method for creating the lenslet 1.0 images is generic, and uses a kernel of 5x5 pixels. That is, a total of 25 multiview images will be transformed into the lenslet 1.0 image.

## Dependencies

The code has the following dependencies:
- [NumPy library](https://numpy.org/install/)
- [Scikit-image library](https://scikit-image.org/docs/stable/install.html)

The code has been tested on MacOS, but should have no problem working on Windows or Linux.

