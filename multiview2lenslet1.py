import numpy as np
from skimage.io import imread, imsave
from skimage.metrics import peak_signal_noise_ratio as psnr

image1 = [None]*25
image1[0] = imread("Cam_000_image_001.png")
image1[1] = imread("Cam_000_image_002.png")
image1[2] = imread("Cam_000_image_003.png")
image1[3] = imread("Cam_000_image_004.png")
image1[4] = imread("Cam_000_image_005.png")
image1[5] = imread("Cam_000_image_006.png")
image1[6] = imread("Cam_000_image_007.png")
image1[7] = imread("Cam_000_image_008.png")
image1[8] = imread("Cam_000_image_009.png")
image1[9] = imread("Cam_000_image_010.png")
image1[10] = imread("Cam_000_image_011.png")
image1[11] = imread("Cam_000_image_012.png")
image1[12] = imread("Cam_000_image_013.png")
image1[13] = imread("Cam_000_image_014.png")
image1[14] = imread("Cam_000_image_015.png")
image1[15] = imread("Cam_000_image_016.png")
image1[16] = imread("Cam_000_image_017.png")
image1[17] = imread("Cam_000_image_018.png")
image1[18] = imread("Cam_000_image_019.png")
image1[19] = imread("Cam_000_image_020.png")
image1[20] = imread("Cam_000_image_021.png")
image1[21] = imread("Cam_000_image_022.png")
image1[22] = imread("Cam_000_image_023.png")
image1[23] = imread("Cam_000_image_024.png")
image1[24] = imread("Cam_000_image_025.png")

image2 = [None]*25
image2[0] = imread("Cam_001_image_001.png")
image2[1] = imread("Cam_001_image_002.png")
image2[2] = imread("Cam_001_image_003.png")
image2[3] = imread("Cam_001_image_004.png")
image2[4] = imread("Cam_001_image_005.png")
image2[5] = imread("Cam_001_image_006.png")
image2[6] = imread("Cam_001_image_007.png")
image2[7] = imread("Cam_001_image_008.png")
image2[8] = imread("Cam_001_image_009.png")
image2[9] = imread("Cam_001_image_010.png")
image2[10] = imread("Cam_001_image_011.png")
image2[11] = imread("Cam_001_image_012.png")
image2[12] = imread("Cam_001_image_013.png")
image2[13] = imread("Cam_001_image_014.png")
image2[14] = imread("Cam_001_image_015.png")
image2[15] = imread("Cam_001_image_016.png")
image2[16] = imread("Cam_001_image_017.png")
image2[17] = imread("Cam_001_image_018.png")
image2[18] = imread("Cam_001_image_019.png")
image2[19] = imread("Cam_001_image_020.png")
image2[20] = imread("Cam_001_image_021.png")
image2[21] = imread("Cam_001_image_022.png")
image2[22] = imread("Cam_001_image_023.png")
image2[23] = imread("Cam_001_image_024.png")
image2[24] = imread("Cam_001_image_025.png")

image2rvs = [None]*25
image2rvs[0] = imread("Cam_001_image_001_rvs.png")
image2rvs[1] = imread("Cam_001_image_002_rvs.png")
image2rvs[2] = imread("Cam_001_image_003_rvs.png")
image2rvs[3] = imread("Cam_001_image_004_rvs.png")
image2rvs[4] = imread("Cam_001_image_005_rvs.png")
image2rvs[5] = imread("Cam_001_image_006_rvs.png")
image2rvs[6] = imread("Cam_001_image_007_rvs.png")
image2rvs[7] = imread("Cam_001_image_008_rvs.png")
image2rvs[8] = imread("Cam_001_image_009_rvs.png")
image2rvs[9] = imread("Cam_001_image_010_rvs.png")
image2rvs[10] = imread("Cam_001_image_011_rvs.png")
image2rvs[11] = imread("Cam_001_image_012_rvs.png")
image2rvs[12] = imread("Cam_001_image_013.png")
image2rvs[13] = imread("Cam_001_image_014_rvs.png")
image2rvs[14] = imread("Cam_001_image_015_rvs.png")
image2rvs[15] = imread("Cam_001_image_016_rvs.png")
image2rvs[16] = imread("Cam_001_image_017_rvs.png")
image2rvs[17] = imread("Cam_001_image_018_rvs.png")
image2rvs[18] = imread("Cam_001_image_019_rvs.png")
image2rvs[19] = imread("Cam_001_image_020_rvs.png")
image2rvs[20] = imread("Cam_001_image_021_rvs.png")
image2rvs[21] = imread("Cam_001_image_022_rvs.png")
image2rvs[22] = imread("Cam_001_image_023_rvs.png")
image2rvs[23] = imread("Cam_001_image_024_rvs.png")
image2rvs[24] = imread("Cam_001_image_025_rvs.png")

def average_psnr(image, image_rvs):
    average = 0
    for i in range(25):
        if not i == 12:
            average = average + psnr(image[i], image_rvs[i])
    average = average/24
    return average

def create_lenslet_1(multiview):
    lenslet = np.zeros((multiview[0].shape[0]*5, multiview[0].shape[1]*5, multiview[0].shape[2]), dtype = multiview[0].dtype)
    for k in range(len(multiview)):
        for i in range(multiview[k].shape[0]):
            for j in range(multiview[k].shape[1]):
                lenslet[k//5+i*5, k%5+j*5] = multiview[k][i,j]
    return lenslet

psnr_im = average_psnr(image2, image2rvs)
print(f"Average PSNR for the 24 RVS-synthesized images: {psnr_im}")

lenslet_cam_000 = create_lenslet_1(image1)
lenslet_cam_001 = create_lenslet_1(image2)
lenslet_cam_001_rvs = create_lenslet_1(image2rvs)

imsave("Cam_000_lenslet1.png", lenslet_cam_000)
imsave("Cam_001_lenslet1.png", lenslet_cam_001)
imsave("Cam_001_lenslet1_rvs.png", lenslet_cam_001_rvs)

psnr_lenslet = psnr(lenslet_cam_001, lenslet_cam_001_rvs)
print(f"PSNR between the two Lenslet 1.0: {psnr_lenslet}")

print("Done!")